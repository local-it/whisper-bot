from nio import AsyncClient, MatrixRoom, RoomMessageAudio
from dotenv import load_dotenv
import asyncio
import requests
import os

def audio_callback_closure(client: AsyncClient):
    async def audio_callback(room: MatrixRoom, event: RoomMessageAudio) -> None:
        print(
            f"Audio received in room {room.display_name}\n"
            f"{room.user_name(event.sender)} | {event.body} | {event.url}"
        )
        with open("completed.txt",'r') as file:
            completed_urls = file.readlines()
        if event.url in completed_urls:
            return
        print("Download audio")
        audio = await client.download(event.url)
        files = {'audio_file': audio.body}
        #with open("/tmp/audiofile",'bw') as file:
        #    file.write(audio.body)
        print("Transcribe audio")
        response = requests.post('http://127.0.0.1:9000/asr?encode=true&language=de&task=transcribe&word_timestamps=false&output=txt', files=files)
        print("Finished transcribing")
        room_id = room.room_id
        body = f"{event.body} \n{response.content.decode()}"
        await client.room_send(
            room_id=room_id,
            message_type="m.room.message",
            content={"msgtype": "m.text", "body": body},
        )
        completed_urls.append(event.url)
        with open("completed.txt",'w') as file:
            file.writelines(completed_urls)

    return audio_callback


async def main() -> None:
    load_dotenv()
    homeserver = str(os.getenv("HOMESERVER"))
    user = str(os.getenv("USER_ID"))
    password = str(os.getenv("PASSWORD"))
    print(f"login at {homeserver} with {user}")
    client = AsyncClient(homeserver, user)
    client.add_event_callback(audio_callback_closure(client), RoomMessageAudio)

    print(await client.login(password))
    # "Logged in as @alice:example.org device id: RANDOMDID"

    await client.sync()
    for room in client.invited_rooms:
        await client.join(room)
        print(f"joined room {room}")
    # Watch out! If you join an old room you'll see lots of old messages
    await client.sync_forever(timeout=30000)  # milliseconds

asyncio.run(main())
