```
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt

docker compose up
python3 whisper.py
```

TODO: use noicecancelling
https://github.com/jneem/nnnoiseless?tab=readme-ov-file
https://github.com/werman/noise-suppression-for-voice

Temperature: 0
https://community.openai.com/t/whisper-hallucination-how-to-recognize-and-solve/218307/16

Prompt: The sentence may be cut off, do not make up words to fill in the rest of the sentence.
